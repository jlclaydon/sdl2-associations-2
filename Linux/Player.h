#ifndef PLAYER_H_
#define PLAYER_H_

#include "SDL2Common.h"
#include "Sprite.h"

// Forward declerations
// improve compile time. 
class Vector2f;
class Animation;

class Player : public Sprite
{
private:
    
    // Sprite information
    static const int SPRITE_HEIGHT = 64;
    static const int SPRITE_WIDTH = 32;
    
    // Animation state
    int state;

public:
    Player();
    ~Player();

    // Player Animation states
    enum PlayerState{LEFT, RIGHT, UP, DOWN, IDLE};

    void init(SDL_Renderer *renderer);

    void processInput(const Uint8 *keyStates);

    int getCurrentAnimationState();
};

#endif