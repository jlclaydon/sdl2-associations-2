﻿#include "NPC.h"


NPC::NPC() : Sprite() {
    game = nullptr;
	state = IDLE;
	speed = 10.0f;

	targetRectangle.w = SPRITE_WIDTH;
	targetRectangle.h = SPRITE_HEIGHT;
}

NPC::~NPC() {

}

int NPC::getCurrentAnimationState() {
	//printf("getCurrentAnimationState() called from npc. Return value was: %d\n", state);
	return state;
}

void NPC::init(SDL_Renderer* renderer)
{

    string path("assets/images/undeadking1.png");
    Vector2f position(180.0f, 200.0f);

    Sprite::init(renderer, path, 5, &position);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i] = new Animation();
    }

    // Setup the animation structure
    animations[LEFT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
    animations[RIGHT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
    animations[UP]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
    animations[DOWN]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
    animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);

    for (int i = 0; i < maxAnimations; i++)
    {
        animations[i]->setMaxFrameTime(0.2f);
    }

}


void NPC::update(float timeDeltaInSeconds)
{
    ai();
    Sprite::update(timeDeltaInSeconds);
}




void NPC::ai() {
    Player* player = game->getPlayer();

    Vector2f vectorToPlayer(player->getPosition());

    vectorToPlayer.sub(position);

    float distance = vectorToPlayer.length();

    printf("s: %f\n", distance);

    if (distance < MAX_RANGE) {
        printf("contact!\n");
    }
    else {

       // float x = vectorToPlayer.getX();
      // float y = vectorToPlayer.getY();

       // printf("x: %f, y: %f\n", x, y);

        velocity->assign(&vectorToPlayer);
        

        // will work but 'wobbles'
        //velocity->scale(speed);

        // Arrive pattern
        distance /= 2;
        
        if (velocity->length() > speed)
        {
            velocity->normalise();
            velocity->scale(speed);
        }
        
       

        state = IDLE;
        if (velocity->getY() > 0.1f)
            state = DOWN;
        else
            if (velocity->getY() < -0.1f) state = UP;
        
        if (velocity->getX() > 0.1f) state = RIGHT;
        else
            if (velocity->getX() < -0.1f) state = LEFT;
        // empty else is not an error.
    }
    
}

void NPC::setGame(Game* game) {
    this->game = game;
}
