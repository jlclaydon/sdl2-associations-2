#ifndef NPC_H_
#define NPC_H_

#include "SDL2Common.h"
#include "Sprite.h"
#include "Game.h"
#include "Vector2f.h"
#include "Animation.h"

class Vector2f;
class Animation;
class Game;

class NPC : public Sprite 
{
private:

	Game* game;

	int state;

	static const int SPRITE_HEIGHT = 64;
	static const int SPRITE_WIDTH = 32;
	static const int MAX_RANGE = 5;

public:
	NPC();
	~NPC();

	enum NPC_state {LEFT, RIGHT, UP, DOWN, IDLE};

	void init(SDL_Renderer* renderer);
	void update(float timeDeltaInSeconds);

	void ai();


	void assign(Vector2f* player);

	int getCurrentAnimationState();

	void setGame(Game* game);
};

#endif
